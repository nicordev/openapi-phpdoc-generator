<?php

declare(strict_types=1);

namespace Nicordev\OpenApiPhpdocGenerator;

class OpenApiPhpdocGenerator
{
    private const NODE_TYPE_SCALAR = 0;
    private const NODE_TYPE_ENUM = 1;
    private const NODE_TYPE_OBJECT = 2;
    private const NODE_TYPE_ARRAY = 3;
    private const NODE_TYPE_ARRAY_ITEM = 4;
    private const NODE_TYPE_ROOT_OBJECT = 5;
    private const NODE_TYPE_ROOT_ARRAY = 6;

    /**
     * @param array<mixed> $responseParts
     */
    public function generate(array $responseParts, int $startingLevel = 0, bool $isInMiddleOfExistingPhpDoc = true): string
    {
        // Look if the root element is an array or an object
        $firstKey = array_key_first($responseParts);
        $rootNodeType = is_int($firstKey) ? self::NODE_TYPE_ROOT_ARRAY : self::NODE_TYPE_ROOT_OBJECT;

        $result = (true === $isInMiddleOfExistingPhpDoc) ? "" : "/**\n";
        $result .= $this->generateNode($firstKey, $responseParts, $startingLevel, $rootNodeType);

        // remove last \n
        $result = substr($result, 0, -1);

        return $result . (true === $isInMiddleOfExistingPhpDoc ? "" : "\n */");
    }

    /**
     * @param mixed $key
     * @param mixed $value
     */
    private function guessNodeType($key, $value): int
    {
        if (false === is_array($value)) {
            return self::NODE_TYPE_SCALAR;
        }

        if (true === is_int($key)) {
            return self::NODE_TYPE_ARRAY_ITEM;
        }

        if (true === is_int(array_key_first($value)) && false === is_array($value[array_key_first($value)])) {
            return self::NODE_TYPE_ENUM;
        }

        if (true === is_int(array_key_first($value))) {
            return self::NODE_TYPE_ARRAY;
        }

        return self::NODE_TYPE_OBJECT;
    }

    /**
     * @param mixed $key
     * @param mixed $value
     */
    private function generateNode($key, $value, int $level = 0, int $nodeType = -1): string
    {
        if ($nodeType < self::NODE_TYPE_SCALAR || $nodeType > self::NODE_TYPE_ROOT_ARRAY) {
            throw new \RuntimeException('Unknown node type: ' . $nodeType);
        }

        if (self::NODE_TYPE_SCALAR === $nodeType) {
            $propertyType = gettype($value);

            return ' * ' . str_repeat(' ', $level * 4) . sprintf('@OA\Property(type="%s", property="%s", example="%s"),', $propertyType, $key, $value) . "\n";
        }

        $nextLevel = $level + 1;
        $property = '';

        switch ($nodeType) {
            case self::NODE_TYPE_ENUM:
                $items = implode('", "', $value);
                $property .= ' * ' . str_repeat(' ', $level * 4) . "@OA\Property(\n";
                $property .= ' * ' . str_repeat(' ', $nextLevel * 4) . "type=\"array\",\n";
                $property .= ' * ' . str_repeat(' ', $nextLevel * 4) . "property=\"$key\",\n";
                $property .= ' * ' . str_repeat(' ', $nextLevel * 4) . "example={\"$items\"},\n";
                $property .= ' * ' . str_repeat(' ', $nextLevel * 4) . sprintf('@OA\Items(type="%s"),', gettype($value[array_key_first($value)])) . "\n";
                $property .= ' * ' . str_repeat(' ', $level * 4) . "),\n";

                return $property;
            case self::NODE_TYPE_OBJECT:
                $property .= ' * ' . str_repeat(' ', $level * 4) . "@OA\Property(\n";
                $property .= ' * ' . str_repeat(' ', $nextLevel * 4) . "type=\"object\",\n";
                $property .= ' * ' . str_repeat(' ', $nextLevel * 4) . "property=\"$key\",\n";
                break;
            case self::NODE_TYPE_ARRAY:
                $property .= ' * ' . str_repeat(' ', $level * 4) . "@OA\Property(\n";
                $property .= ' * ' . str_repeat(' ', $nextLevel * 4) . "type=\"array\",\n";
                $property .= ' * ' . str_repeat(' ', $nextLevel * 4) . "property=\"$key\",\n";
                $property .= ' * ' . str_repeat(' ', $nextLevel * 4) . "@OA\Items(\n";
                break;
            case self::NODE_TYPE_ROOT_OBJECT:
                $nextLevel = $level;
                break;
            case self::NODE_TYPE_ROOT_ARRAY:
                $property .= ' * ' . str_repeat(' ', $level * 4) . "@OA\Items(\n";
                $nextLevel = $level;
                break;
        }

        foreach ($value as $innerKey => $innerValue) {
            $nextNodeType = $this->guessNodeType($innerKey, $innerValue);
            $property .= $this->generateNode($innerKey, $innerValue, $nextLevel, $nextNodeType);
        }

        if (false === in_array($nodeType, [self::NODE_TYPE_ROOT_OBJECT, self::NODE_TYPE_ROOT_ARRAY])) {
            $property .= ' * ' . str_repeat(' ', $level * 4) . "),\n";
        }

        return $property;
    }
}
