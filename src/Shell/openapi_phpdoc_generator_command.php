#! /usr/bin/php
<?php

const NODE_TYPE_SCALAR = 0;
const NODE_TYPE_ENUM = 1;
const NODE_TYPE_OBJECT = 2;
const NODE_TYPE_ARRAY = 3;
const NODE_TYPE_ARRAY_ITEM = 4;
const NODE_TYPE_ROOT_OBJECT = 5;
const NODE_TYPE_ROOT_ARRAY = 6;

/**
 * @param mixed $key
 * @param mixed $value
 */
function guessNodeType($key, $value): int
{
    if (false === is_array($value)) {
        return NODE_TYPE_SCALAR;
    }

    if (true === is_int($key)) {
        return NODE_TYPE_ARRAY_ITEM;
    }

    if (true === is_int(array_key_first($value)) && false === is_array($value[array_key_first($value)])) {
        return NODE_TYPE_ENUM;
    }

    if (true === is_int(array_key_first($value))) {
        return NODE_TYPE_ARRAY;
    }

    return NODE_TYPE_OBJECT;
}

/**
 * @param mixed $key
 * @param mixed $value
 */
function generateNode($key, $value, int $level = 0, int $nodeType = -1): string
{
    if ($nodeType < NODE_TYPE_SCALAR || $nodeType > NODE_TYPE_ROOT_ARRAY) {
        throw new RuntimeException('Unknown node type: ' . $nodeType);
    }

    if (NODE_TYPE_SCALAR === $nodeType) {
        $propertyType = gettype($value);

        return ' * ' . str_repeat(' ', $level * 4) . sprintf('@OA\Property(type="%s", property="%s", example="%s"),', $propertyType, $key, $value) . "\n";
    }

    $nextLevel = $level + 1;
    $property = '';

    switch ($nodeType) {
        case NODE_TYPE_ENUM:
            $items = implode('", "', $value);
            $property .= ' * ' . str_repeat(' ', $level * 4) . "@OA\Property(\n";
            $property .= ' * ' . str_repeat(' ', $nextLevel * 4) . "type=\"array\",\n";
            $property .= ' * ' . str_repeat(' ', $nextLevel * 4) . "property=\"$key\",\n";
            $property .= ' * ' . str_repeat(' ', $nextLevel * 4) . "example={\"$items\"},\n";
            $property .= ' * ' . str_repeat(' ', $nextLevel * 4) . sprintf('@OA\Items(type="%s"),', gettype($value[array_key_first($value)])) . "\n";
            $property .= ' * ' . str_repeat(' ', $level * 4) . "),\n";

            return $property;
        case NODE_TYPE_OBJECT:
            $property .= ' * ' . str_repeat(' ', $level * 4) . "@OA\Property(\n";
            $property .= ' * ' . str_repeat(' ', $nextLevel * 4) . "type=\"object\",\n";
            $property .= ' * ' . str_repeat(' ', $nextLevel * 4) . "property=\"$key\",\n";
            break;
        case NODE_TYPE_ARRAY:
            $property .= ' * ' . str_repeat(' ', $level * 4) . "@OA\Property(\n";
            $property .= ' * ' . str_repeat(' ', $nextLevel * 4) . "type=\"array\",\n";
            $property .= ' * ' . str_repeat(' ', $nextLevel * 4) . "property=\"$key\",\n";
            $property .= ' * ' . str_repeat(' ', $nextLevel * 4) . "@OA\Items(\n";
            break;
        case NODE_TYPE_ROOT_OBJECT:
            $nextLevel = $level;
            break;
        case NODE_TYPE_ROOT_ARRAY:
            $property .= ' * ' . str_repeat(' ', $level * 4) . "@OA\Items(\n";
            $nextLevel = $level;
            break;
    }

    foreach ($value as $innerKey => $innerValue) {
        $nextNodeType = guessNodeType($innerKey, $innerValue);
        $property .= generateNode($innerKey, $innerValue, $nextLevel, $nextNodeType);
    }

    if (false === in_array($nodeType, [NODE_TYPE_ROOT_OBJECT, NODE_TYPE_ROOT_ARRAY])) {
        $property .= ' * ' . str_repeat(' ', $level * 4) . "),\n";
    }

    return $property;
}

/**
 * @param array<mixed> $responseParts
 */
function generate(array $responseParts, int $startingLevel = 0, bool $isInMiddleOfExistingPhpDoc = true): string
{
    // Look if the root element is an array or an object
    $firstKey = array_key_first($responseParts);
    $rootNodeType = is_int($firstKey) ? NODE_TYPE_ROOT_ARRAY : NODE_TYPE_ROOT_OBJECT;

    $result = (true === $isInMiddleOfExistingPhpDoc) ? "" : "/**\n";
    $result .= generateNode($firstKey, $responseParts, $startingLevel, $rootNodeType);

    // remove last \n
    $result = substr($result, 0, -1);

    return $result . (true === $isInMiddleOfExistingPhpDoc ? "" : "\n */");
}

define('SCRIPT_NAME', basename(array_shift($argv)));

if (empty($argv[0]) && empty($DEBUG_MODE_ACTIVATED)) {
    echo "IS_IN_MIDDLE=1 " . SCRIPT_NAME . " \e[33myourJsonFileHere [startDepthLevel]\e[0m\n";
    exit;
}

$jsonFile = $argv[0];
$startDepthLevel = $argv[1] ?? 0;
$isInMiddleOfExistingPhpDoc = $_SERVER['IS_IN_MIDDLE'] ?? 1;
$responseParts = json_decode((string) file_get_contents($jsonFile), true, 512, JSON_THROW_ON_ERROR);

if (false === is_array($responseParts)) {
    echo "The json content needs to be either an object or an array.\n";
    exit;
}

echo generate($responseParts, $startDepthLevel, (bool) $isInMiddleOfExistingPhpDoc);
