ifdef filter
	FILTER = --filter ${filter}
endif

ifdef level
	LEVEL = --level ${level}
endif

composer-install-using-docker: ## Install development libraries
	docker run --rm --interactive --tty --volume $$PWD:/app --workdir /app composer:2 composer install

test-using-docker: ## Run tests using docker. To target specific test cases: make test-using-docker filter=ourFilterCriteriaHere
	docker run --rm --interactive --tty --volume $$PWD:/tmp --workdir /tmp php:7.4 php vendor/bin/phpunit test --filter=${filter}

test: ## Run tests. To target specific test cases: make test filter=ourFilterCriteriaHere
	php vendor/bin/phpunit test ${FILTER}

phpstan: ## Run phpstan
	php vendor/bin/phpstan analyse src test ${LEVEL}

.DEFAULT_GOAL := help
help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

.PHONY: help test test-using-docker
