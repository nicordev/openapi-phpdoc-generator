# OpenApi phpdoc generator

Generate OpenApi phpdoc from json content.

## Installation

Here we assume that you have php installed locally under `/usr/bin`:

```bash
cp src/Shell/openapi_phpdoc_generator_command.php /usr/local/bin/openapi_phpdoc_generator # To copy the command into one of our $PATH directory
chmod +x src/Shell/openapi_phpdoc_generator_command.php /usr/local/bin # To be able to execute the script without adding php in front of the command name
```

## Usage

In your terminal, run:

```bash
openapi_phpdoc_generator
```

Then you will get some explanation about how to use the command, like this:

```bash
[IS_IN_MIDDLE=1] php openapi_phpdoc_generator yourJsonFileHere [startingDepthLevelHere]
```

To use the `OpenApiPhpdocGenerator` class in your codebase, use:

```php
use Nicordev\OpenApiPhpdocGenerator\OpenApiPhpdocGenerator;

$ourAwesomeDecodedJsonContent = file_get_contents($ourAwesomeDecodedJsonContentFilePath);
$openApiPhpdocGenerator = new OpenApiPhpdocGenerator();
$result = $openApiPhpdocGenerator->generate($ourDecodedJsonContent, $ourStartingDepthLevel, $isInMiddleOfExistingPhpDoc);
```

I wish you a happy phpdoc generation!

## Test

To run the test suite:
1. `composer install`
2. `make test`

If you're a docker enthusiast and you don't have php installed locally, you can do this instead:
1. `make composer-install-using-docker`
2. `make test-using-docker`

## PhpStan

I've also added PHPStan, you can use it using `make` commands.