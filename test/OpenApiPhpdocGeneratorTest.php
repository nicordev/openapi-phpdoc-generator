<?php

declare(strict_types=1);

namespace Nicordev\Test;

use Nicordev\OpenApiPhpdocGenerator\OpenApiPhpdocGenerator;
use PHPUnit\Framework\TestCase;

class OpenApiPhpdocGeneratorTest extends TestCase
{
    public function testGenerate_withRootObject(): void
    {
        $expectedPhpdoc = file_get_contents(__DIR__.'/expected/testGenerate_withRootObject');
        $jsonContentParts = json_decode((string) file_get_contents(__DIR__ . '/fixtures/dummy_root_object.json'), true, 512, JSON_THROW_ON_ERROR);
        $openApiPhpdocGenerator = new OpenApiPhpdocGenerator();
        $result = $openApiPhpdocGenerator->generate($jsonContentParts, 0, true);
        self::assertEquals($expectedPhpdoc, $result);
    }

    public function testGenerate_withRootArray(): void
    {
        $expectedPhpdoc = file_get_contents(__DIR__.'/expected/testGenerate_withRootArray');
        $jsonContentParts = json_decode((string) file_get_contents(__DIR__ . '/fixtures/dummy_root_array.json'), true, 512, JSON_THROW_ON_ERROR);
        $openApiPhpdocGenerator = new OpenApiPhpdocGenerator();
        $result = $openApiPhpdocGenerator->generate($jsonContentParts, 0, true);
        self::assertEquals($expectedPhpdoc, $result);
    }

    public function testGenerate_withRootObjectStartingLevel3(): void
    {
        $expectedPhpdoc = file_get_contents(__DIR__.'/expected/testGenerate_withRootObjectStartingLevel3');
        $jsonContentParts = json_decode((string) file_get_contents(__DIR__ . '/fixtures/dummy_root_object.json'), true, 512, JSON_THROW_ON_ERROR);
        $openApiPhpdocGenerator = new OpenApiPhpdocGenerator();
        $result = $openApiPhpdocGenerator->generate($jsonContentParts, 3, true);
        self::assertEquals($expectedPhpdoc, $result);
    }

    public function testGenerate_withRootArrayStartingLevel3(): void
    {
        $expectedPhpdoc = file_get_contents(__DIR__.'/expected/testGenerate_withRootArrayStartingLevel3');
        $jsonContentParts = json_decode((string) file_get_contents(__DIR__ . '/fixtures/dummy_root_array.json'), true, 512, JSON_THROW_ON_ERROR);
        $openApiPhpdocGenerator = new OpenApiPhpdocGenerator();
        $result = $openApiPhpdocGenerator->generate($jsonContentParts, 3, true);
        self::assertEquals($expectedPhpdoc, $result);
    }

    public function testGenerate_NotInMiddle(): void
    {
        $expectedPhpdoc = file_get_contents(__DIR__.'/expected/testGenerate_withRootObject');
        $jsonContentParts = json_decode((string) file_get_contents(__DIR__ . '/fixtures/dummy_root_object.json'), true, 512, JSON_THROW_ON_ERROR);
        $openApiPhpdocGenerator = new OpenApiPhpdocGenerator();
        $result = $openApiPhpdocGenerator->generate($jsonContentParts, 0, false);
        self::assertEquals("/**\n".$expectedPhpdoc."\n */", $result);
    }
}