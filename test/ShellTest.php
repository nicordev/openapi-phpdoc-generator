<?php

declare(strict_types=1);

namespace Nicordev\Test;

use PHPUnit\Framework\TestCase;

class ShellTest extends TestCase
{
    public function testGenerate_withRootObject(): void
    {
        $expectedPhpdoc = file_get_contents(__DIR__.'/expected/testGenerate_withRootObject');
        $result = shell_exec('IS_IN_MIDDLE=1 php '.__DIR__.'/../src/Shell/openapi_phpdoc_generator_command.php '.__DIR__.'/fixtures/dummy_root_object.json');
        self::assertEquals($expectedPhpdoc, $result);
    }

    public function testGenerate_withRootArray(): void
    {
        $expectedPhpdoc = file_get_contents(__DIR__.'/expected/testGenerate_withRootArray');
        $result = shell_exec('IS_IN_MIDDLE=1 php '.__DIR__.'/../src/Shell/openapi_phpdoc_generator_command.php '.__DIR__.'/fixtures/dummy_root_array.json');
        self::assertEquals($expectedPhpdoc, $result);
    }

    public function testGenerate_withRootObjectStartingLevel3(): void
    {
        $expectedPhpdoc = file_get_contents(__DIR__.'/expected/testGenerate_withRootObjectStartingLevel3');
        $result = shell_exec('IS_IN_MIDDLE=1 php '.__DIR__.'/../src/Shell/openapi_phpdoc_generator_command.php '.__DIR__.'/fixtures/dummy_root_object.json 3');
        self::assertEquals($expectedPhpdoc, $result);
    }

    public function testGenerate_withRootArrayStartingLevel3(): void
    {
        $expectedPhpdoc = file_get_contents(__DIR__.'/expected/testGenerate_withRootArrayStartingLevel3');
        $result = shell_exec('IS_IN_MIDDLE=1 php '.__DIR__.'/../src/Shell/openapi_phpdoc_generator_command.php '.__DIR__.'/fixtures/dummy_root_array.json 3');
        self::assertEquals($expectedPhpdoc, $result);
    }

    public function testGenerate_NotInMiddle(): void
    {
        $expectedPhpdoc = file_get_contents(__DIR__.'/expected/testGenerate_withRootObject');
        $result = shell_exec('IS_IN_MIDDLE=0 php '.__DIR__.'/../src/Shell/openapi_phpdoc_generator_command.php '.__DIR__.'/fixtures/dummy_root_object.json');
        self::assertEquals("/**\n".$expectedPhpdoc."\n */", $result);
    }
}